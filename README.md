# Språksamlingen - docs

Jeg forsøker å samle aktuelle ontologier og vokabular i dette repo slik at det er lett for alle å hente de ned lokalt.

## Eksperiment med ODA og ODAC

Se et diagram som viser en direkte konvertering til rdf her:

http://www.ontodia.org/diagram?sharedDiagram=1oomlh5q6mvhmjkllmd1t650tm

I mappen oda-experiment ligger input filer, et feilet forsøk på å konvertere til rdf med OpenRefine og et forsøk fom gikk en del bedre.

## Datamodeller:

Se `ontologies` for de jeg har lastet ned.

Jeg har ikke lastet ned LemonOILS, siden jeg ikke helt forstår hvordan LEMON henger sammen med Ontolex. Det ser ut som Ontolex har tatt grunnstrukturen fra LEMON.

* Ontolex, https://www.w3.org/2016/05/ontolex/

```
# Ontolex 
@prefix ontolex: <http://www.w3.org/ns/lemon/ontolex#> .
@prefix synsem: <http://www.w3.org/ns/lemon/synsem#> .
@prefix decomp: <http://www.w3.org/ns/lemon/decomp#> .
@prefix vartrans: <http://www.w3.org/ns/lemon/vartrans#> .
@prefix lime: <http://www.w3.org/ns/lemon/lime#> .

# Standard ontologier
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix owl: <http://www.w3.org/2002/07/owl#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.

# SKOS for konsept og emneregister
@prefix skos: <http://www.w3.org/2004/02/skos#>.

# DBpedia for betydning
@prefix dbr: <http://dbpedia.org/resource/>.
@prefix dbo: <http://dbpedia.org/ontology/>.

# VoID for å beskrive datasett og relasjoner mellom de
@prefix void: <http://rdfs.org/ns/void#>.

# Konsept for lingvistikk
@prefix lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>.

# Semiotics og oils, usikker hva det brukes til
@prefix semiotics: <http://www.ontologydesignpatterns.org/cp/owl/semiotics.owl#>.
@prefix oils: <http://lemon-model.net/oils#>.

# DCTerms for basic ting som tittel
@prefix dct: <http://purl.org/dc/terms/>.

# PROV for utsagn om provenans
@prefix provo: <http://www.w3.org/ns/prov#>.
```

## Diverse:

* https://linguistics.okfn.org/
* https://docs.google.com/spreadsheets/d/1iyXce-HUBGEJC6mVM7RxeF9SDdrHshN7f_a0CzjwFl4/edit?authkey=CJi9u78D&hl=en_US&authkey=CJi9u78D&hl=en_US#gid=0
