# BLL – Linguistic Linked Open Data Edition

http://data.linguistik.de/bll/index.html

> The Bibliography of Linguistic Literature (BLL) is one of the most comprehensive linguistic bibliographies worldwide. It covers general linguistics with all its neighboring disciplines and subdomains as well as English, German and Romance linguistics. The BLL dates back as far as 1971 and, as of November 2017, lists circa 460.000 bibliographic references. Furthermore, the BLL provides a hierarchically categorized thesaurus of domain-specific index terms in German and English. The BLL Thesaurus comprises more than 7.900 subject terms.
