# General Ontology for Linguistic Description (GOLD)

http://linguistics-ontology.org/version

http://linguistics-ontology.org/gold/2010

> XML:
> A basic XML version of the GOLD ontology listing each concept with its label and definition. Other attributes given for each concept include the concept's canonical URI and reference to its parent in order to provide a hierarchical representation of the ontology tree.
> OWL:
> GOLD was originally developed with the semantic web in mind and can be used as a vocabulary for describing linguistic data in RDF format. An example implementation of GOLD that uses more of OWL's ontological features is being maintained by Scott Farrar at the University of Washington (see: http://code.google.com/p/goldcomm/).
