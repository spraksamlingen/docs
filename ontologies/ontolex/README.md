# Ontolex

https://www.w3.org/2016/05/ontolex/

> This document describes the lexicon model for ontologies (lemon) as a main outcome of the work of the Ontology Lexicon (Ontolex) community group.
>
> Ontologies are an important component of the Semantic Web but current ontology languages such as OWL and RDF(S) lack support for enriching ontologies with linguistic information, in particular with information concerning how ontology entities, i.e. properties, classes, individuals, etc. can be realized in natural language. The model described in this document aims to close this gap by providing a vocabulary that allows ontologies to be enriched with information about how the vocabulary elements described in them are realized linguistically, in particular in natural languages.
>
> OWL and RDF(S) rely on the RDFS lable property to capture the relation between a vocabulary element and its (preferred) lexicalization in a given language. This lexicalization provides a lexical anchor that makes the class, property, individual etc. understandable to a human user. The use of a simple label for linguistic grounding as available in OWL and RDF(S) is far from being able to capture the necessary linguistic and lexical information that Natural Language Processing (NLP) applications working with a particular ontology need.
>
> The aim of lemon is to provide rich linguistic grounding for ontologies. Rich linguistic grounding includes the representation of morphological and syntactic properties of lexical entries as well as the syntax-semantics interface, i.e. the meaning of these lexical entries with respect to an ontology or vocabulary.
> ///
> Ontology-lexicon interface (ontolex)
> Syntax and Semantics (synsem)
> Decomposition (decomp)
> Variation and Translation (vartrans)
> Linguistic Metadata (lime)

